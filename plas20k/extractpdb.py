import os
import shutil

# set the path to the folder containing the .pdb files
folder_path = "/data/users/d4/public_html/plas20k/protein-ligand"

# set the path to the output folder where the subfolders will be created
output_folder_path = "/data/users/d4/public_html/plas20k/output"

# loop over all files in the folder
for filename in os.listdir(folder_path):
    # check if the file is a .pdb file
    if filename.endswith(".pdb"):
        # extract the name of the file without the extension
        name = os.path.splitext(filename)[0]
        # create a new folder with the same name as the file (without extension) in the output folder
        new_folder_path = os.path.join(output_folder_path, name)
        os.makedirs(new_folder_path, exist_ok=True)
        # copy the file to the new folder
        shutil.copy(os.path.join(folder_path, filename), new_folder_path)
